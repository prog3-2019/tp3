package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import model.Person.Role;

public class TeamGenerator extends Thread {

	private ArrayList<Person> people;
	private ArrayList<Person> finalTeam;
	private ArrayList<Pair<Person, Person>> incompatiblePeople;
	private int[] minimum;
	private int[] maximum;

	public TeamGenerator() {
		people = new ArrayList<Person>();
		incompatiblePeople = new ArrayList<Pair<Person, Person>>();
		minimum = new int[4];
		maximum = new int[4];
	}

	public void initialize() {
		setInitialPeople();
		setIncompatibilities();
	}

	@Override
	public void run() {
		finalTeam = sortByRole(generateTeam());
	}

	private ArrayList<Person> generateTeam() {
		ArrayList<Person> solution = new ArrayList<Person>();
		ArrayList<Person> incompatibles = new ArrayList<Person>();
		for (Person p : people) {
			if (canBeAdded(p, solution)) {
				solution.add(p);
			} else {
				if (!incompatibles.contains(p))
					incompatibles.add(p);
			}
		}
		return pickBestSolution(generateTeam(solution, incompatibles, new ArrayList<ArrayList<Person>>()));
	}

	private boolean canBeAdded(Person p, ArrayList<Person> list) {
		boolean ret = true;
		for (Pair<Person, Person> pair : incompatiblePeople) {
			ret = ret && !(pair.getV1().equals(p) || pair.getV2().equals(p));
		}
		ret = ret && countPeopleWithThisRole(p.getRole(), list) + 1 <= maximum[detectRole(p)];
		return ret;
	}

	private int countPeopleWithThisRole(Role role, ArrayList<Person> list) {
		int cont = 0;
		for (Person p : list) {
			if (p.getRole().equals(role))
				cont++;
		}
		return cont;
	}

	private ArrayList<Person> pickBestSolution(ArrayList<ArrayList<Person>> solutions) {
		ArrayList<Person> best = new ArrayList<Person>();
		for (ArrayList<Person> sol : solutions) {
			if (sol.size() > best.size() && isAValidSolution(sol) && sol != null)
				best = sol;
		}
		return best;
	}

	private boolean isAValidSolution(ArrayList<Person> solution) {
		int pls = countPeopleWithThisRole(Role.PL, solution);
		int archs = countPeopleWithThisRole(Role.ARCHITECT, solution);
		int progs = countPeopleWithThisRole(Role.PROGRAMMER, solution);
		int tests = countPeopleWithThisRole(Role.TESTER, solution);
		return pls >= minimum[0] && pls <= maximum[0] && archs >= minimum[1] && archs <= maximum[1]
				&& progs >= minimum[2] && progs <= maximum[2] && tests >= minimum[3] && tests <= maximum[3]
				&& !hasIncompatibles(solution);
	}

	private boolean hasIncompatibles(ArrayList<Person> list) {
		for (Person p1 : list) {
			for (Person p2 : list) {
				if (areIncompatibles(p1, p2))
					return true;
			}
		}
		return false;
	}

	public boolean areIncompatibles(Person p1, Person p2) {
		boolean ret = false;
		if (!incompatiblePeople.isEmpty()) {
			for (Pair<Person, Person> p : incompatiblePeople) {
				ret = ret || ((p.getV1().equals(p1) && p.getV2().equals(p2))
						|| (p.getV1().equals(p2) && p.getV2().equals(p1)));
			}
		}
		return ret;
	}

	private ArrayList<ArrayList<Person>> generateTeam(ArrayList<Person> base, ArrayList<Person> incompatibles,
			ArrayList<ArrayList<Person>> solutions) {
		if (!couldBeASolution(base)) {
			return null;
		} else {
			for (Person p : incompatibles) {
				if (!base.contains(p)) {
					base.add(p);
					if (generateTeam(base, incompatibles, solutions) == null) {
						base.remove(p);
						solutions.add(base);
					} else {
						solutions = generateTeam(base, incompatibles, solutions);
					}
				}
			}
		}
		return solutions;
	}

	private boolean couldBeASolution(ArrayList<Person> solution) {
		int pls = countPeopleWithThisRole(Role.PL, solution);
		int archs = countPeopleWithThisRole(Role.ARCHITECT, solution);
		int progs = countPeopleWithThisRole(Role.PROGRAMMER, solution);
		int tests = countPeopleWithThisRole(Role.TESTER, solution);
		return pls <= maximum[0] && archs <= maximum[1] && progs <= maximum[2] && tests <= maximum[3]
				&& !hasIncompatibles(solution);
	}

	private void setInitialPeople() {
		people.add(new Person("Karen Garazzino", Role.PL));
		people.add(new Person("Jorgelina Rial", Role.PL));
		people.add(new Person("Andres Rojas", Role.PL));
		people.add(new Person("Martin Di Salvo", Role.PL));
		people.add(new Person("Mauro Lombardo", Role.PL));
		people.add(new Person("Alejandro Nelis", Role.ARCHITECT));
		people.add(new Person("Fernando Torres", Role.ARCHITECT));
		people.add(new Person("Lucas Adissi", Role.ARCHITECT));
		people.add(new Person("Luis Veronessi", Role.ARCHITECT));
		people.add(new Person("Jorge Tenca", Role.ARCHITECT));
		people.add(new Person("Nicolas Cespede", Role.PROGRAMMER));
		people.add(new Person("Juan Molina", Role.PROGRAMMER));
		people.add(new Person("Maximiliano Sandoval", Role.PROGRAMMER));
		people.add(new Person("Maximiliano Peralta", Role.PROGRAMMER));
		people.add(new Person("Tomas Altamirano", Role.PROGRAMMER));
		people.add(new Person("Ivan Sayko", Role.PROGRAMMER));
		people.add(new Person("Emanuel Tenca", Role.PROGRAMMER));
		people.add(new Person("Tobias Dolezor", Role.PROGRAMMER));
		people.add(new Person("Geronimo Benavides", Role.PROGRAMMER));
		people.add(new Person("Belen Cabezas", Role.PROGRAMMER));
		people.add(new Person("Gonzalo Banzas", Role.PROGRAMMER));
		people.add(new Person("Nahuel Vazquez", Role.PROGRAMMER));
		people.add(new Person("Hvara Ocar", Role.TESTER));
		people.add(new Person("Marcelo Wasinger", Role.TESTER));
		people.add(new Person("Leandro Sanchez", Role.TESTER));
		people.add(new Person("Franco Chamorro", Role.TESTER));
		people.add(new Person("Tomas Montenegro", Role.TESTER));
		people.add(new Person("Leandro Tenca", Role.TESTER));
		people.add(new Person("Francisco Postiglione", Role.TESTER));
		people.add(new Person("Facundo Banzas", Role.TESTER));
		Collections.shuffle(people);
	}

	private void setIncompatibilities() {
		Random gen = new Random();
		for (int i = 0; i < 15; i++) {
			Person p1 = people.get(gen.nextInt(people.size()));
			Person p2 = people.get(gen.nextInt(people.size()));
			if (!p1.equals(p2) && !areIncompatibles(p1, p2))
				incompatiblePeople.add(new Pair<Person, Person>(p1, p2));
			else
				i--;
		}
	}

	public void setLimits(int role, int minimum, int maximum) {
		if (role >= 0 && role <= 3 && minimum > 0 && maximum > 0) {
			this.minimum[role] = minimum;
			this.maximum[role] = maximum;
		}
	}

	private int detectRole(Person p) {
		if (p.getRole() == Role.PL)
			return 0;
		if (p.getRole() == Role.ARCHITECT)
			return 1;
		if (p.getRole() == Role.PROGRAMMER)
			return 2;
		return 3;
	}

	public void addPerson(Person person) {
		if (exists(person.getName()))
			searchPerson(person.getName()).setRole(person.getRole());
		else if (!people.contains(person))
			people.add(person);
	}

	private boolean exists(String name) {
		for (Person p : people) {
			if (p.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public void addIncompatibility(Person p1, Person p2) {
		Pair<Person, Person> pair1 = new Pair<Person, Person>(p1, p2);
		Pair<Person, Person> pair2 = new Pair<Person, Person>(p2, p1);
		if (p1 != null && p2 != null) {
			if (!incompatiblePeople.contains(pair1) && !incompatiblePeople.contains(pair2))
				incompatiblePeople.add(pair1);
		} else
			throw new RuntimeException("Parametros invalidos");
	}

	public Person searchPerson(String name) {
		for (Person p : people) {
			if (p.getName().equals(name))
				return p;
		}
		return null;
	}

	public ArrayList<Person> getPeople() {
		return people;
	}

	public String incompatibleWith(Person p) {
		StringBuilder sb = new StringBuilder();
		for (Pair<Person, Person> pair : incompatiblePeople) {
			if (pair.getV1().equals(p))
				sb.append(", " + pair.getV2().getName());
			else if (pair.getV2().equals(p))
				sb.append(", " + pair.getV1().getName());
		}
		sb.delete(0, 1);
		return sb.toString();
	}

	public ArrayList<Person> getTeam() {
		return finalTeam;
	}

	public ArrayList<Pair<Person, Person>> getincompatiblePeople() {
		return this.incompatiblePeople;
	}

	public int[] getMax() {
		return this.maximum;
	}

	public int[] getMin() {
		return this.minimum;
	}
	public ArrayList<Person> sortByRole(ArrayList<Person> team) {
		ArrayList<Person>copy=new ArrayList<Person>(team);
		copy.sort(new Comparator<Person>() {
			@Override
			public int compare(Person p1,Person p2) {
				return p1.compareTo(p2);
			}
		});
		return copy;
	}	
}