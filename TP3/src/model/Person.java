package model;

public class Person implements Comparable<Person> {

	public enum Role {
		PL, ARCHITECT, PROGRAMMER, TESTER
	}

	private String name;
	private Role role;

	public Person(String name, Role role) {
		if (name != null && !name.equals("") && role != null) {
			this.name = name;
			this.role = role;
		} else {
			throw new RuntimeException("Parametros invalidos.");
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (role != other.role)
			return false;
		return true;
	}
	 @Override
     public int compareTo(Person o) {
         if (this.role==Role.PL && (o.role==Role.ARCHITECT || o.role==Role.PROGRAMMER || o.role==Role.TESTER)) {
             return -1;
         }
         if (this.role==Role.ARCHITECT && o.role==Role.PL) {
             return 1;
         }
         if(this.role==Role.ARCHITECT && (o.role==Role.PROGRAMMER || o.role!=Role.TESTER)) {
        	 return -1;
         }
         if(this.role==Role.PROGRAMMER &&(o.role==Role.ARCHITECT || o.role==Role.PL)) {
        	 return 1;
         }
         if(this.role==Role.PROGRAMMER &&(o.role==Role.TESTER)) {
        	 return -1;
         }
         if(this.role==Role.TESTER && ( o.role==Role.PL|| o.role==Role.ARCHITECT || o.role==Role.PROGRAMMER)) {
        	 return 1;
         }
         return 0;
     }

	@Override
	public String toString() {
		return "Persona: [Nombre: " + name + ", Rol: " + role + "]";
	}

	// SETTERS & GETTERS
	public String getName() {
		return name;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
