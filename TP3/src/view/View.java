package view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import model.TeamGenerator;
import model.Person;
import model.Person.Role;
import javax.swing.JTextPane;

public class View {

	private JFrame frame;
	private TeamGenerator teamGenerator;
	private JTable table;
	private DefaultTableModel tableModel;
	private JScrollPane scrollPane;
	private JTextPane textPane;
	private ArrayList<JTextField> textFields;
	private ArrayList<JLabel> labels;
	private JComboBox<String> roleComboBox;
	private ArrayList<JInternalFrame> internalFrames;
	private ArrayList<JButton> buttons;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View window = new View();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public View() {
		/*
		 * try { UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); }
		 * catch (Exception e) { System.out.println("Error setting native LAF: " + e); }
		 */
		initialize();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initialize() {
		teamGenerator = new TeamGenerator();
		teamGenerator.initialize();

		frame = new JFrame();
		frame.setTitle("Generador de equipos de trabajo");
		frame.setResizable(false);
		frame.setBounds(0, 0, 1366, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		internalFrames = new ArrayList<JInternalFrame>();
		for (int i = 0; i < 3; i++) {
			internalFrames.add(new JInternalFrame("Error"));
		}
		internalFrames.add(new JInternalFrame("Equipo completo"));
		internalFrames.get(0).setBounds(652, 486, 273, 104);
		internalFrames.get(1).setBounds(950, 482, 257, 108);
		internalFrames.get(2).setBounds(420, 482, 300, 108);
		internalFrames.get(3).setBounds(458, 50, 330, 501);
		internalFrames.get(3).setResizable(true);
		for (JInternalFrame jif : internalFrames) {
			jif.setClosable(true);
			jif.getContentPane().setLayout(null);
			frame.getContentPane().add(jif);
			jif.setVisible(false);
		}

		textFields = new ArrayList<JTextField>();
		for (int i = 0; i < 8; i++) {
			textFields.add(new JTextField());
			if (i < 4)
				textFields.get(i).setToolTipText("m\u00EDnimo");
			else
				textFields.get(i).setToolTipText("m\u00E1ximo");
		}
		for (int i = 0; i < 3; i++) {
			textFields.add(new JTextField());
			textFields.get(i + 8).setToolTipText("nombre y apellido de la persona");
		}
		textFields.get(0).setBounds(476, 622, 25, 20);
		textFields.get(1).setBounds(428, 647, 25, 20);
		textFields.get(2).setBounds(451, 672, 25, 20);
		textFields.get(3).setBounds(405, 697, 25, 20);
		textFields.get(4).setBounds(519, 622, 25, 20);
		textFields.get(5).setBounds(475, 647, 25, 20);
		textFields.get(6).setBounds(496, 672, 25, 20);
		textFields.get(7).setBounds(451, 697, 25, 20);
		textFields.get(8).setBounds(800, 622, 113, 20);
		textFields.get(9).setBounds(1092, 622, 113, 20);
		textFields.get(10).setBounds(1092, 647, 113, 20);
		for (JTextField tf : textFields) {
			tf.setColumns(10);
			frame.getContentPane().add(tf);
		}

		labels = new ArrayList<JLabel>();
		labels.add(new JLabel("Configuraci\u00F3n para el armado de equipos:"));
		labels.add(new JLabel("L\u00EDderes de proyecto: Entre             y"));
		labels.add(new JLabel("Arquitectos: Entre             y"));
		labels.add(new JLabel("Programadores: Entre             y"));
		labels.add(new JLabel("Testers: Entre             y"));
		labels.add(new JLabel("Agregar persona:"));
		labels.add(new JLabel("Nombre:"));
		labels.add(new JLabel("Rol:"));
		labels.add(new JLabel("Agregar incompatibilidad:"));
		labels.add(new JLabel("Nombre de persona 1:"));
		labels.add(new JLabel("Nombre de persona 2:"));
		labels.add(new JLabel("(los nombres deben"));
		labels.add(new JLabel("ser exactos)"));
		labels.add(new JLabel("Escriba nombres v\u00E1lidos"));
		labels.add(new JLabel("Seleccione un rol"));
		labels.add(new JLabel("y escriba un nombre v\u00E1lido"));
		labels.add(new JLabel("Debe llenar los criterios correctamente"));
		labels.get(0).setFont(new Font("Tahoma", Font.BOLD, 11));
		labels.get(5).setFont(new Font("Tahoma", Font.BOLD, 11));
		labels.get(8).setFont(new Font("Tahoma", Font.BOLD, 11));
		labels.get(13).setFont(new Font("Tahoma", Font.PLAIN, 15));
		labels.get(14).setFont(new Font("Tahoma", Font.PLAIN, 15));
		labels.get(15).setFont(new Font("Tahoma", Font.PLAIN, 15));
		labels.get(16).setFont(new Font("Tahoma", Font.PLAIN, 15));
		labels.get(0).setBounds(282, 600, 243, 14);
		labels.get(1).setBounds(316, 625, 205, 14);
		labels.get(2).setBounds(316, 650, 170, 14);
		labels.get(3).setBounds(316, 675, 185, 14);
		labels.get(4).setBounds(316, 700, 160, 14);
		labels.get(5).setBounds(723, 600, 113, 14);
		labels.get(6).setBounds(744, 625, 57, 14);
		labels.get(7).setBounds(744, 650, 48, 14);
		labels.get(8).setBounds(950, 600, 169, 14);
		labels.get(9).setBounds(960, 625, 136, 14);
		labels.get(10).setBounds(960, 650, 136, 14);
		labels.get(11).setBounds(1215, 625, 120, 14);
		labels.get(12).setBounds(1237, 650, 105, 14);
		labels.get(13).setBounds(34, 28, 183, 22);
		labels.get(14).setBounds(68, 11, 119, 19);
		labels.get(15).setBounds(40, 30, 207, 22);
		labels.get(16).setBounds(25, 30, 275, 22);
		for (JLabel jl : labels)
			frame.getContentPane().add(jl);
		internalFrames.get(1).getContentPane().add(labels.get(13));
		internalFrames.get(0).getContentPane().add(labels.get(14));
		internalFrames.get(0).getContentPane().add(labels.get(15));
		internalFrames.get(2).getContentPane().add(labels.get(16));

		buttons = new ArrayList<JButton>();
		buttons.add(new JButton("Armar equipo"));
		buttons.add(new JButton("Agregar"));
		buttons.add(new JButton("Agregar"));
		buttons.get(0).setBounds(550, 660, 120, 23);
		buttons.get(1).setBounds(744, 685, 89, 23);
		buttons.get(2).setBounds(1007, 685, 89, 23);
		buttons.get(0).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					detectLimits();
					teamGenerator.run();
					updateTextPane();
				} catch (NumberFormatException ex) {
					internalFrames.get(2).setVisible(true);
				}
			}
		});
		buttons.get(1).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					teamGenerator.addPerson(new Person(textFields.get(8).getText(), detectRoleFromComboBox()));
					updateTable();
				} catch (RuntimeException exception) {
					internalFrames.get(0).setVisible(true);
				}
			}
		});
		buttons.get(2).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					teamGenerator.addIncompatibility(teamGenerator.searchPerson(textFields.get(9).getText()),
							teamGenerator.searchPerson(textFields.get(10).getText()));
					updateTable();
				} catch (RuntimeException exception) {
					internalFrames.get(1).setVisible(true);
				}
			}
		});
		for (JButton jb : buttons)
			frame.getContentPane().add(jb);

		textPane = new JTextPane();
		textPane.setBounds(0, 0, 314, 471);
		textPane.setEditable(false);
		internalFrames.get(3).getContentPane().add(textPane);

		roleComboBox = new JComboBox<String>();
		roleComboBox.setToolTipText("");
		roleComboBox.setBounds(770, 645, 143, 22);
		frame.getContentPane().add(roleComboBox);
		roleComboBox.setModel(new DefaultComboBoxModel<String>(
				new String[] { "(seleccione un rol)", "Lider de proyecto", "Arquitecto", "Programador", "Tester" }));

		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 1360, 589);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);
		tableModel = new DefaultTableModel(new Object[][] {},
				new String[] { "Nombre y apellido", "Rol", "Incompatibilidades" }) {
			private static final long serialVersionUID = 1L;
			Class[] columnTypes = new Class[] { String.class, String.class, String.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			boolean[] columnEditables = new boolean[] { false, true, true };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		};
		table.setModel(tableModel);
		table.getColumnModel().getColumn(0).setPreferredWidth(200);
		table.getColumnModel().getColumn(1).setPreferredWidth(75);
		table.getColumnModel().getColumn(2).setPreferredWidth(300);
		completeTable();
	}

	private void detectLimits() {
		for (int i = 0; i < 4; i++) {
			if (Integer.parseInt(textFields.get(i).getText()) < 1)
				throw new NumberFormatException("Valor inv�alido");
			else
				teamGenerator.setLimits(i, Integer.parseInt(textFields.get(i).getText()),
						Integer.parseInt(textFields.get(i + 4).getText()));
		}
	}

	private Role detectRoleFromComboBox() {
		if (roleComboBox.getSelectedIndex() == 1)
			return Role.PL;
		if (roleComboBox.getSelectedIndex() == 2)
			return Role.ARCHITECT;
		if (roleComboBox.getSelectedIndex() == 3)
			return Role.PROGRAMMER;
		if (roleComboBox.getSelectedIndex() == 4)
			return Role.TESTER;
		return null;
	}

	private void completeTable() {
		for (Person p : teamGenerator.getPeople()) {
			tableModel.addRow(new String[] { p.getName(), p.getRole().toString(), teamGenerator.incompatibleWith(p) });
		}
	}

	private void updateTable() {
		for (int i = tableModel.getRowCount() - 1; i >= 0; i--) {
			tableModel.removeRow(i);
		}
		completeTable();
	}

	private void updateTextPane() {
		StringBuilder sb = new StringBuilder();
		if (teamGenerator.getTeam().isEmpty())
			sb.append("No se encontro una solucion.");
		else {
			sb.append("Su equipo ha quedado conformado por:\n");
			for (Person p : teamGenerator.getTeam()) {
				sb.append(p.getName() + " (" + p.getRole().toString() + ")\n");
			}
		}
		textPane.setText(sb.toString());
		internalFrames.get(3).setVisible(true);
	}
}
