package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import model.TeamGenerator;
import model.Pair;
import model.Person;
import model.Person.Role;

class Tests {
	private TeamGenerator e, e1;
	private Person p1, p2, p3, p4, p5, p6;

	@BeforeEach
	void initialize() {
		e = new TeamGenerator();
		e1 = new TeamGenerator();

		p1 = new Person("Juan Molina", Role.PROGRAMMER);
		p2 = new Person("Nicolas Céspede", Role.TESTER);
		p3 = new Person("Axel Molina", Role.PL);
		p4 = new Person("Andres Rojas", Role.ARCHITECT);
		p5 = new Person("Mauro Lombardo", Role.PL);
		p6 = new Person("Martin Di Salvo", Role.ARCHITECT);

		e1.addPerson(p1);
		e1.addPerson(p1);
		e1.addPerson(p2);
		e1.addPerson(p2);
		e1.addPerson(p3);
		e1.addPerson(p4);
		e1.addPerson(p5);
		e1.addPerson(p6);
	}

	@Test
	void contructorTest() {
		assertEquals("[]", e.getPeople().toString());
	}

	@Test
	void contructorTest1() {
		assertEquals("[]", e.getincompatiblePeople().toString());
	}

	@Test
	void contructorTest2() {
		int cont = 0;
		for (int i = 0; i < e.getMax().length; i++) {
			cont += e.getMax()[i];
		}
		assertEquals(0, cont);
	}

	@Test
	void contructorTest3() {
		int cont = 0;
		for (int i = 0; i < e.getMin().length; i++) {
			cont += e.getMin()[i];
		}
		assertEquals(0, cont);
	}

	@Test
	void addPersonTest() {
		assertEquals(6, e1.getPeople().size());

	}

	@Test
	void setTestLimitsTest() {
		int cont = 0;
		e.setLimits(0, 1, 1);
		e.setLimits(-1, 3, 4);
		e.setLimits(2, -1, 3);
		e.setLimits(3, 4, -2);
		e.setLimits(4, 5, 8);
		cont += e.getMin()[0];
		cont += e.getMax()[0];
		cont += e.getMin()[1];
		cont += e.getMax()[1];
		cont += e.getMin()[2];
		cont += e.getMax()[2];
		cont += e.getMin()[3];
		cont += e.getMax()[3];

		assertEquals(2, cont);

	}

	@Test
	void addIncompatibilityTest1() {
		assertThrows(RuntimeException.class, () -> e1.addIncompatibility(null, null));
	}

	@Test
	void addIncompatibilityTest2() {
		assertThrows(RuntimeException.class, () -> e1.addIncompatibility(p1, null));
	}

	@Test
	void addIncompatibilityTest3() {
		assertThrows(RuntimeException.class, () -> e1.addIncompatibility(null, p2));
	}

	@Test
	void addIncompatibilityTest4() {
		e1.addIncompatibility(p1, p2);
		e1.addIncompatibility(p1, p2);
		e1.addIncompatibility(p2, p1);
		assertEquals(1, e1.getincompatiblePeople().size());
	}

	@Test
	void incompatibleWithTest() {
		StringBuffer st = new StringBuffer(" Nicolas Céspede, Axel Molina, Andres Rojas");
		e1.addIncompatibility(p1, p2);
		e1.addIncompatibility(p1, p3);
		e1.addIncompatibility(p4, p1);
		e1.addIncompatibility(p4, p5);
		assertEquals(st.toString(), e1.incompatibleWith(p1));
	}

	@Test
	void searchPersonTest1() {
		assertNull(e1.searchPerson("Tirri"));
	}

	@Test
	void searchPersonTest2() {
		assertEquals(new Person("Juan Molina", Role.PROGRAMMER), e1.searchPerson("Juan Molina"));
	}

	@Test
	void areIncompatiblesTest1() {
		e1.addIncompatibility(p1, p2);
		assertFalse(e1.areIncompatibles(p3, p2));
	}

	@Test
	void areIncompatiblesTest2() {
		assertFalse(e1.areIncompatibles(p3, p2));
	}

	@Test
	void areIncompatiblesTest3() {
		e1.addIncompatibility(p1, p2);
		assertTrue(e1.areIncompatibles(p1, p2));
	}

	@Test
	void areIncompatiblesTest4() {
		e1.addIncompatibility(p1, p2);
		assertTrue(e1.areIncompatibles(p2, p1));
	}

	@Test
	void areIncompatiblesTest5() {
		e1.addIncompatibility(p4, p5);
		e1.addIncompatibility(p1, p2);
		e1.addIncompatibility(p3, p1);
		e1.addIncompatibility(p2, p4);
		e1.addIncompatibility(p1, p6);
		assertFalse(e1.areIncompatibles(p4, p3));
	}

	@Test
	void areIncompatiblesTest6() {
		e1.addIncompatibility(p4, p5);
		e1.addIncompatibility(p1, p2);
		e1.addIncompatibility(p3, p1);
		e1.addIncompatibility(p2, p4);
		e1.addIncompatibility(p1, p6);
		assertFalse(e1.areIncompatibles(p3, p5));
	}

	@Test
	void areIncompatiblesTest7() {
		e1.addIncompatibility(p4, p5);
		e1.addIncompatibility(p1, p2);
		e1.addIncompatibility(p3, p1);
		e1.addIncompatibility(p2, p4);
		e1.addIncompatibility(p1, p6);
		assertTrue(e1.areIncompatibles(p4, p5));
	}

	@Test
	void runTest1() {
		e1.addIncompatibility(p1, p3);
		e1.addIncompatibility(p4, p2);
		e1.addIncompatibility(p2, p3);

		e1.setLimits(0, 1, 1);
		e1.setLimits(1, 1, 1);
		e1.setLimits(2, 1, 1);
		e1.setLimits(3, 2, 3);
		e1.run();
		assertEquals(0, e1.getTeam().size());

	}

	@Test
	void runTest2() {
		TeamGenerator e2 = new TeamGenerator();
		Person p1 = new Person("Juan", Role.PROGRAMMER);
		Person p2 = new Person("Nico", Role.ARCHITECT);
		Person p3 = new Person("Maxi", Role.TESTER);
		Person p4 = new Person("Ivan", Role.PROGRAMMER);
		Person p5 = new Person("Axel", Role.ARCHITECT);
		Person p6 = new Person("Mauricio", Role.PL);
		Person p7 = new Person("Toby", Role.TESTER);
		Person p8 = new Person("Pedro", Role.PROGRAMMER);
		Person p9 = new Person("Martin", Role.PL);
		Person p10 = new Person("Ysy a", Role.TESTER);
		Person p11 = new Person("Tony", Role.PL);
		Person p12 = new Person("Duki", Role.PROGRAMMER);
		e2.addPerson(p1);
		e2.addPerson(p2);
		e2.addPerson(p3);
		e2.addPerson(p4);
		e2.addPerson(p5);
		e2.addPerson(p6);
		e2.addPerson(p7);
		e2.addPerson(p8);
		e2.addPerson(p9);
		e2.addPerson(p10);
		e2.addPerson(p11);
		e2.addPerson(p12);
		e2.addIncompatibility(p1, p3);
		e2.addIncompatibility(p4, p2);
		e2.addIncompatibility(p3, p7);
		e2.addIncompatibility(p11, p6);
		e2.addIncompatibility(p2, p8);

		e2.setLimits(0, 1, 1);
		e2.setLimits(1, 1, 2);
		e2.setLimits(2, 1, 3);
		e2.setLimits(3, 2, 2);
		e2.run();
		assertEquals(7, e2.getTeam().size());
	}

	@Test
	void initializeTest() {
		e.initialize();
		assertEquals(30, e.getPeople().size());
	}

	@Test
	void PersonTest() {
		assertThrows(RuntimeException.class, () -> new Person(null, null));
	}

	@Test
	void PersonTest2() {
		assertThrows(RuntimeException.class, () -> new Person("Juan", null));
	}

	@Test
	void PersonTest3() {
		assertThrows(RuntimeException.class, () -> new Person(null, Role.ARCHITECT));
	}

	@Test
	void PersonTest4() {
		assertThrows(RuntimeException.class, () -> new Person("", Role.ARCHITECT));
	}

	@Test
	void PersonToStringTest() {
		assertEquals("Persona: [Nombre: Juan Molina, Rol: PROGRAMMER]", p1.toString());
	}

	@Test
	void pairtoStringTest() {
		Pair<Person, Person> pair = new Pair<Person, Person>(p1, p2);
		String str = "[Persona: [Nombre: Juan Molina, Rol: PROGRAMMER]";
		String str2 = "Persona: [Nombre: Nicolas Céspede, Rol: TESTER]]";
		assertEquals(str + ", " + str2, pair.toString());
	}
}
